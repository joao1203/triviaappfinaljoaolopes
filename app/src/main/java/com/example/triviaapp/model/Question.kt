package com.example.triviaapp.model

data class Question(
    val Theme: String,
    val question: String,
    val answer1: String,
    val answer2: String,
    val answer3: String,
    val answer4: String,
    val correct: String
)
