package com.example.triviaapp.model

import com.example.triviaapp.model.Question

class Questions {
    companion object{
        val themes: MutableList<MutableList<Question>> = mutableListOf(
            mutableListOf<Question>(
                Question("geography","Which is the largest desert in the world?","Antartic Desert","Sahara Desert","Arabian Desert","Artic Desert","Antartic Desert"),
                Question("geography","What is the capital of Canada?","Ottawa","Toronto","Ontario","Vancouver","Ottawa"),
                Question("geography","How many time zones does Russia have?","11","4","9","10","11"),
                Question("geography","What planet is closest to Earth?","","","","","MERCURY")
            ),mutableListOf<Question>(
                Question("science","What was the name of the first man-made satellite launched by the Soviet Union in 1957?","Sputnik","Spotnik","Spotnic","Sputnic","Sputnik"),
                Question("science","How many bones are in the human body?","206","204","205","202","206"),
                Question("science","How many bones do sharks have?","0","178","54","32","0"),
                Question("science","Which freezes faster, hot water or cold water?","","","","","HOT WATER")
            ),mutableListOf<Question>(
                Question("art","Who painted the famous painting 'Girl with a Pearl Earring'?","Johannes Vermeer","Anthony van Dyck","Peter Paul Rubens","Michelangelo Merisi da Caravaggio","Johannes Vermeer"),
                Question("art","Which of these is not an artistic period?","Supersurrealism","Toyism","Rococo","Metarealism","Supersurrealism"),
                Question("art","Which of these is not a renaissance artist?","Picasso","Leonardo","Donatello","Michelangelo","Picasso"),
                Question("art","Which Spanish painter is referred to as both the last of the old masters and the moderns? ","","","","","FRANCISCO GOYA")
            ),mutableListOf<Question>(
                Question("cinema","Which movie won the Oscar for Best Animated Feature in 2019?","Spider-Man Into the Spider-Verse","Ralph Breaks the Internet","Isle of Dogs","Incredibles 2","Spider-Man Into the Spider-Verse"),
                Question("cinema","Which movie is the most expensive movie ever?","Pirates of the Caribbean: On Stranger Tides","Avengers: Endgame","Avatar: The Way of Water","Avengers: Infinity War", "Pirates of the Caribbean: On Stranger Tides"),
                Question("cinema","Which of these actors does not have an EGOT?","Emma Stone","Rita Moreno","John Legend","Whoopi Goldberg","Emma Stone"),
                Question("cinema","Write the name of the second part of Avengers 5:","","","","","THE KANG DINASTY")
            ),mutableListOf<Question>(
                Question("history","When was John F. Kennedy assassinated?","1963","1962","1964","1965","1963"),
                Question("history","When did Mao Zedong come to power?","1949","1948","1950","1954","1949"),
                Question("history","What is the name of the first human civilization?","Mesopotamia","Ancient Egypt","Mesoamerica","Ancient China","Mesopotamia"),
                Question("history","Who was the first man to walk on the moon?","","","","","NEIL ARMSTRONG")
            ),mutableListOf<Question>(
                Question("sport","How many times has LeBron James won the NBA MVP award?","4","3","5","2","4"),
                Question("sport","What is the national sport of Canada?","Lacrosse","Hockey","Curling","Snowboarding","Lacrosse"),
                Question("sport","Who is the most followed athlete on Instagram?","Cristiano Ronaldo","LeBron James","Lionel Messi","Neymar Jr.","Cristiano Ronaldo"),
                Question("sport","Which country has won the most World Cups?","","","","","BRAZIL")
            )

        )
    }

}