package com.example.triviaapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.triviaapp.databinding.FragmentMenuBinding
import com.example.triviaapp.databinding.FragmentMenuBinding.inflate

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding
    var userName = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        binding.playbutton.setOnClickListener {
            userName = binding.userName.text.toString()
            if (userName != ""){
                val action =
                    MenuFragmentDirections.actionMenuFragmentToGameScreenFragment(userName)
                findNavController().navigate(action)
            }
            else{
                Toast.makeText(activity,"WARNING! You must enter a name before playing.",Toast.LENGTH_SHORT).show()
            }
        }
        binding.rankbutton.setOnClickListener {
            val action =
                MenuFragmentDirections.actionMenuFragmentToRankFragment()
            findNavController().navigate(action)
        }
    }

}