package com.example.triviaapp.view

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.triviaapp.R
import com.example.triviaapp.databinding.FragmentGameScreenBinding
import com.example.triviaapp.model.Questions

class GameScreenFragment : Fragment(), View.OnClickListener {
    lateinit var binding: FragmentGameScreenBinding
    var rounds: Int = 1
    var times: Int = 0
    var score = 0
    var answer = "Avengers: Endgame"
    val handler = Handler(Looper.getMainLooper())
    var selected = 0
    var chosen = 0
    var order = 0
    var userName = ""
    lateinit var timer: CountDownTimer



    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentGameScreenBinding.inflate(inflater, container, false)
            Questions.themes.shuffle()
            binding.question.text = Questions.themes[selected][0].question
            binding.answer1.text = Questions.themes[selected][0].answer1
            binding.answer2.text = Questions.themes[selected][0].answer2
            binding.answer3.text = Questions.themes[selected][0].answer3
            binding.answer4.text = Questions.themes[selected][0].answer4
            binding.submit.visibility = View.GONE
            binding.writtenanswer.visibility = View.GONE
            answer = Questions.themes[selected][0].correct
            order = (0..3).random()

            userName = arguments?.getString("userName").toString()
            return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.answer1.setOnClickListener(this)
        binding.answer2.setOnClickListener(this)
        binding.answer3.setOnClickListener(this)
        binding.answer4.setOnClickListener(this)
        binding.submit.setOnClickListener(this)

        timer = object : CountDownTimer(20000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.progress.progress--
            }
            override fun onFinish(){
                order = (0..3).random()
                rounds += 1
                chosen++
                if (chosen == 4){
                    chosen = 0
                }

                times++
                if (times == 2){
                    selected++
                    times = 0
                }

                if (rounds != 13){
                    doWhenRound()
                }

                if(rounds >= 13){handler.postDelayed({
                    chosen = 0
                    val action =
                        GameScreenFragmentDirections.actionGameScreenFragmentToResultFragment(score,userName)
                    findNavController().navigate(action) }, 1500)
                }
            }
        }.start()
    }

    override fun onClick(p0: View?){
        timer.cancel()
        binding.answer1.isEnabled = false
        binding.answer2.isEnabled = false
        binding.answer3.isEnabled = false
        binding.answer4.isEnabled = false
        binding.submit.isEnabled = false

        val button = p0 as Button
        var guess = button.text

        order = (0..3).random()
        rounds += 1
        chosen++
        if (chosen == 4){
            chosen = 0
        }

        times++
        if (times == 2){
            selected++
            times = 0
        }

        if(guess == answer && guess != binding.submit.text || binding.writtenanswer.text.toString().uppercase() == answer){
            button.setBackgroundColor(Color.parseColor("#00ff16"))
            button.setTextColor(resources.getColor(android.R.color.white))

            score += binding.progress.progress * 5
            binding.score.text = "Score: $score"
        }
        if(guess != answer && guess != binding.submit.text){
            button.setBackgroundColor(Color.parseColor("#D70040"))
        }
        if(rounds != 13){
            doWhenRound()
            handler.postDelayed({
                button.setBackgroundColor(Color.parseColor("#197b21"))
            },1500)
        }

        if(rounds >= 13){handler.postDelayed({
            chosen = 0
            println(userName)
            println(score)
            val action =
                GameScreenFragmentDirections.actionGameScreenFragmentToResultFragment(score,userName)
            findNavController().navigate(action) }, 1500)
            }
    }

    fun doWhenRound (){
        handler.postDelayed({
            binding.answer1.isEnabled = true
            binding.answer2.isEnabled = true
            binding.answer3.isEnabled = true
            binding.answer4.isEnabled = true
            binding.submit.isEnabled = true
            binding.progress.progress = 20
            binding.question.text = Questions.themes[selected][chosen].question
            when(order){
                0 -> {
                    binding.answer1.text = Questions.themes[selected][chosen].answer1
                    binding.answer2.text = Questions.themes[selected][chosen].answer2
                    binding.answer3.text = Questions.themes[selected][chosen].answer3
                    binding.answer4.text = Questions.themes[selected][chosen].answer4
                }
                1 -> {
                    binding.answer1.text = Questions.themes[selected][chosen].answer4
                    binding.answer2.text = Questions.themes[selected][chosen].answer1
                    binding.answer3.text = Questions.themes[selected][chosen].answer2
                    binding.answer4.text = Questions.themes[selected][chosen].answer3
                }
                2 -> {
                    binding.answer1.text = Questions.themes[selected][chosen].answer3
                    binding.answer2.text = Questions.themes[selected][chosen].answer2
                    binding.answer3.text = Questions.themes[selected][chosen].answer1
                    binding.answer4.text = Questions.themes[selected][chosen].answer4
                }
                3 -> {
                    binding.answer1.text = Questions.themes[selected][chosen].answer2
                    binding.answer2.text = Questions.themes[selected][chosen].answer4
                    binding.answer3.text = Questions.themes[selected][chosen].answer3
                    binding.answer4.text = Questions.themes[selected][chosen].answer1
                }
            }
            binding.writtenanswer.setText("")
            answer = Questions.themes[selected][chosen].correct
            binding.round.text = "Round ${rounds}/12"

            if (Questions.themes[selected][chosen].answer1 == ""){
                binding.answer1.visibility = View.GONE
                binding.answer2.visibility = View.GONE
                binding.answer3.visibility = View.GONE
                binding.answer4.visibility = View.GONE
                binding.submit.visibility = View.VISIBLE
                binding.writtenanswer.visibility = View.VISIBLE
            }
            else{
                binding.answer1.visibility = View.VISIBLE
                binding.answer2.visibility = View.VISIBLE
                binding.answer3.visibility = View.VISIBLE
                binding.answer4.visibility = View.VISIBLE
                binding.submit.visibility = View.GONE
                binding.writtenanswer.visibility = View.GONE
            }
        }, 1500)
        handler.postDelayed({
            timer.start()
        }, 1900)
    }
}