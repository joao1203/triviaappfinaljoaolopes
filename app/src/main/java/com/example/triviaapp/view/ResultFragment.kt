package com.example.triviaapp.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.triviaapp.databinding.FragmentResultBinding
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter

class ResultFragment : Fragment() {
    var name = ""
    var fileName = "data"
    lateinit var binding: FragmentResultBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResultBinding.inflate(inflater, container, false)
        binding.resulttext.text = arguments?.getInt("score").toString()
        name = arguments?.getString("userName").toString()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        writeFile()
        readFile()
        context?.getFileStreamPath(fileName)?.absolutePath
        binding.returnbutton.setOnClickListener {
            val action =
                ResultFragmentDirections.actionResultFragmentToRankFragment()
            findNavController().navigate(action)
        }
        binding.sharebutton.setOnClickListener(){
            val t1 = binding.resulttext.text.toString()
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, "My latest score in Trivia Jungle was ${t1}.")
            startActivity(Intent.createChooser(shareIntent,"Share via"))
        }
    }
    fun writeFile(){
        val file = OutputStreamWriter(requireActivity().openFileOutput(fileName, Activity.MODE_APPEND))
        file.appendLine("$name ${binding.resulttext.text}")
        file.flush()
        file.close()
    }

    fun readFile() {
        var content = ""
        val file = InputStreamReader(requireActivity().openFileInput("data"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content += line + "\n"
            line = br.readLine()
        }
        println(content)
    }

}