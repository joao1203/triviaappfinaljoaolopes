package com.example.triviaapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.triviaapp.databinding.FragmentRankBinding
import java.io.BufferedReader
import java.io.InputStreamReader

class RankFragment : Fragment() {
    lateinit var binding: FragmentRankBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRankBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        readFile()

        binding.menubutton.setOnClickListener {
            val action =
                RankFragmentDirections.actionRankFragmentToMenuFragment()
            findNavController().navigate(action)
        }
    }

    fun readFile() {
        var content = mutableListOf<MutableList<String>>()
        val file = InputStreamReader(requireActivity().openFileInput("data"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content.add(line.split(" ").toMutableList())
            line = br.readLine()
        }
        content.sortByDescending { it[1].toInt() }

        var rank = ""
        var score = ""
        var position = 0

        for (user in content) {
            val userName = user[0]
            val userScore = user[1]
            position++
            rank += "$position. ${userName.capitalize()}\n"
            score += "$userScore\n"
        }

        binding.nameRank.text = rank
        binding.scoreRank.text = score
    }
}